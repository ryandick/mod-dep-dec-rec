var detective = require( 'detective' ),
	fs = require( 'fs' ),
	args = process.argv,
	wrench = require( 'wrench' );

function detect( argument ) {
	var src = fs.readFileSync( argument );
	var requires = detective( src );
	return requires;
}

function walk( path ) {
	var f = wrench.readdirSyncRecursive( srcPath );
	var rR = {};
	console.log( f.length + ' files found' );
	//	for each file
	f.forEach( function ( file ) {
		//	if file is a javascript fike
		if ( file.indexOf( '.js' ) > 0 ) {
			var filepath = srcPath + file;
			console.log( 'scanning for dependencies: ' + filepath );
			//	if not a directory
			if ( filepath.substring( filepath.length - 1, filepath.length ) !== '/' ) {
				//	if not a module
				if ( filepath.indexOf( 'node_modules/' ) <= 0 ) {
					//	if not a clientside module
					if ( filepath.indexOf( 'bower/' ) <= 0 ) {
						//	scan path for dependencies
						var reqMods = detect( filepath );
						//	iterate returned dependencies and add to list
						reqMods.forEach( function ( r ) {
							if ( r.indexOf( './' ) < 0 ) {
								if ( !rR[ r ] ) rR[ r ] = 1;
								else {
									rR[ r ] = rR[ r ] + 1;
								}
							}
						} );
					}
				}
			}
		}
	} );
	//	log results
	console.log( rR );
}
//	arguments
var srcPath = args[ 2 ] || './';
//	instantiation
walk( srcPath );